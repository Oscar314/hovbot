
vel_ramp_rate = 10 #wheelturns/ss
pos_vel_limit = 4 #m/s
pos_acc_limit = 1 #m/ss
pos_decel_limit = 1 #m/ss

turn_bias = 0.5 # Used to determine how stick deflection should affect steering
left_wheel_circ = 501 #mm  
right_wheel_circ = 496
track_width  = 521.4     #mm

vel_gain_I = 5  # Zero to avoid movement after release of stick
vel_gain_P = 5 # Needs to be higher with a heavy load, a human needs around 3-5, without load 0.5 to 1 is good
max_vel = 500  #max Speed in mm/s

input_filter_bandwidth = 2
led_pin = 7 #Pin on RPI to be pulled high once every meter traveled
reset_pin = 11
