import numpy as np
import heapq
from scipy.ndimage import distance_transform_edt
import math

class A_star():
    def heuristic(self, a, b):
        return np.abs(a[0] - b[0]) + np.abs(a[1] - b[1])
    
    def astar(self, array, start, goal):
    #Created with chatpgt using the query: Create an A star implementation in python that use a numpy array as map and two points as start and finish

        # Define the cost function
        # Define the start and goal nodes
        start_node = (start[0], start[1])
        goal_node = (goal[0], goal[1])
        if array[start_node] != 0:
            print("Moving first waypoint!")
            window_size = 5
            window_half = window_size // 2
            window = array[max(start_node[0]-window_half, 0):start_node[0]+window_half+1,
                            max(start_node[1]-window_half, 0):start_node[1]+window_half+1]
            mask = (window == 0)
            distances = distance_transform_edt(~mask)

            # Find the position with the minimum distance
            min_dist_pos = np.argwhere(distances == distances.min())[0]
            closest_pos = (start_node[0] - window_half + min_dist_pos[0],
                           start_node[1] - window_half + min_dist_pos[1])

            print(f"Start pos: {start_node}, closest pos: {closest_pos}")
            start_node = closest_pos
            #print(array[start_node])

        if array[goal_node] != 0:
            print("Moving last waypoint!")
            window_size = 5
            window_half = window_size // 2
            window = array[max(goal_node[0]-window_half, 0):goal_node[0]+window_half+1,
                            max(goal_node[1]-window_half, 0):goal_node[1]+window_half+1]
            mask = (window == 0)
            distances = distance_transform_edt(~mask)

            # Find the position with the minimum distance
            min_dist_pos = np.argwhere(distances == distances.min())[0]
            closest_pos = (goal_node[0] - window_half + min_dist_pos[0],
                           goal_node[1] - window_half + min_dist_pos[1])

            print(f"Start pos: {goal_node}, closest pos: {closest_pos}")
            goal_node = closest_pos
            #print(array[start_node])


        edge_distance = 6
        distance_matrix = np.zeros(shape=(edge_distance*2+1,edge_distance*2+1))
        for y, x in np.array(np.meshgrid(np.arange(start=0, stop = edge_distance*2+1, step=1), np.arange(start=0, stop = edge_distance*2+1, step=1))).T.reshape(-1,2):
            distance_matrix[y, x] = max(0, (edge_distance+1)**2-round((y-edge_distance)**2 + (x-edge_distance)**2))

        distances = np.zeros(shape=(600,600))
        for y, x in np.argwhere(array > 0):
            if y - edge_distance >= 0 and y+edge_distance < 600 and x-edge_distance >= 0 and x+edge_distance < 600:
                #print(np.maximum(distances[y-edge_distance:y+edge_distance+1, x-edge_distance:x+edge_distance+1], distance_matrix))
                distances[y-edge_distance:y+edge_distance+1, x-edge_distance:x+edge_distance+1] = np.maximum(distances[y-edge_distance:y+edge_distance+1, x-edge_distance:x+edge_distance+1], distance_matrix)
                #print(distances[y-edge_distance:y+edge_distance+1, x-edge_distance:x+edge_distance+1])

        # Initialize the frontier with the start node
        frontier = [(0, start_node)]
        heapq.heapify(frontier)

        # Initialize the came_from dictionary and the cost_so_far dictionary
        came_from = {}
        cost_so_far = {}
        came_from[start_node] = None
        cost_so_far[start_node] = 0
        delta = 1




        # Start the search
        while len(frontier) > 0:
            # Pop the node with the lowest priority
            current_cost, current_node = heapq.heappop(frontier)

            # Check if we have reached the goal
            if self.heuristic(goal_node, current_node) < delta:
                goal_node = current_node
                break

            # Explore the neighbors of the current node
            for next_node in [(0, -delta), (0, delta), (-delta, 0), (delta, 0)]:
                neighbor = (current_node[0] + next_node[0], current_node[1] + next_node[1])
                if array[neighbor] < 5:
                    # Compute the new cost for the neighbor
                    new_cost = cost_so_far[current_node] + delta + distances[neighbor]#min(10, distances[neighbor])
                    # Check if the neighbor has not been visited or if the new cost is lower than the previous one
                    if neighbor not in cost_so_far or new_cost < cost_so_far[neighbor]:
                        # Update the cost_so_far and came_from dictionaries
                        cost_so_far[neighbor] = new_cost
                        priority = new_cost + self.heuristic(goal_node, neighbor)
                        heapq.heappush(frontier, (priority, neighbor))
                        came_from[neighbor] = current_node

            for next_node in [(delta,delta), (-delta,delta), (delta, -delta), (-delta, -delta)]:
                neighbor = (current_node[0] + next_node[0], current_node[1] + next_node[1])
                if array[neighbor] < 5:
                    # Compute the new cost for the neighbor
                    new_cost = cost_so_far[current_node] + delta*math.sqrt(2) + distances[neighbor]#min(10, distances[neighbor])
                    # Check if the neighbor has not been visited or if the new cost is lower than the previous one
                    if neighbor not in cost_so_far or new_cost < cost_so_far[neighbor]:
                        # Update the cost_so_far and came_from dictionaries
                        cost_so_far[neighbor] = new_cost
                        priority = new_cost + self.heuristic(goal_node, neighbor)
                        heapq.heappush(frontier, (priority, neighbor))
                        came_from[neighbor] = current_node

        # Reconstruct the path from start to goal
        path = [goal_node]
        counter = 0
        while path[-1] != start_node:
            counter += 1
            if path[-1] in came_from:
                path.append(came_from[path[-1]])
            else:
                print(f"returned -1 at point:{counter}")
                return -1
        path.reverse()
        #print(path)
        return path