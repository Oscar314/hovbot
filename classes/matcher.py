import open3d as o3d
import numpy as np
import math


class Matcher():
    def __init__(self, use_pre_scanned_map, pre_scanned_map):
        self.size = 600
        self.use_pre_scanned_map = use_pre_scanned_map
        self.pcd = o3d.geometry.PointCloud()

        if self.use_pre_scanned_map:
            self.pcd.points = o3d.utility.Vector3dVector(pre_scanned_map)
        else:
            self.regular_map = np.zeros(shape=(self.size, self.size))
            self.a_star_safe_drive = np.ones(shape=(self.size, self.size))
            self.a_star_safe_drive[3:-3,3:-3] = 0

            for i in range(220, 320):
                self.regular_map[i][200] = 100
                self.a_star_safe_drive[i][200] = 100


        self.a_star_padding_radius = 10
        self.a_star_pre_calculated_padding_matrix = np.zeros(shape=(self.a_star_padding_radius*2+1, self.a_star_padding_radius*2+1))
        a_star_x, a_star_y = np.meshgrid(np.arange(self.a_star_padding_radius*2+1), np.arange(self.a_star_padding_radius*2+1))
        a_star_dist = np.hypot(a_star_x-self.a_star_padding_radius, a_star_y-self.a_star_padding_radius)
        a_star_mask = a_star_dist <= self.a_star_padding_radius
        self.a_star_pre_calculated_padding_matrix[a_star_mask] = 1

    
    def rotate(self,x,y,xo,yo,theta):
        xr=math.cos(theta)*(x-xo)-math.sin(theta)*(y-yo)   + xo
        yr=math.sin(theta)*(x-xo)+math.cos(theta)*(y-yo)  + yo
        return [xr,yr]

    def convert_scan(self, scan, pose):
        pts = []
        for d, angle in scan:
            x =  pose[0] + d*math.cos(angle - pose[2])
            y = -pose[1] + d*math.sin(angle - pose[2])
            
            pts.append(np.array([x, y, 0]))

        return np.array(pts)
    
    def add_scan(self, pose, angle_distance):
        pts = self.convert_scan(angle_distance, pose)
        return  self.scan_match(pts, pose)


    def update_gridmap(self, source):
        new_points = []
        for p in source:
            x = min(599, max(0, 300+int(p[0]/50)))
            y  = min(599, max(0, 300-int(p[1]/50)))
            if self.regular_map[y,x] == 0:
                new_points.append(p)


            x1_a_star = min(599, max(0, x-self.a_star_padding_radius))
            x2_a_star = max(0, min(599, x+self.a_star_padding_radius+1))
            y1_a_star = min(599, max(0, y-self.a_star_padding_radius))
            y2_a_star = max(0, min(599, y+self.a_star_padding_radius+1))
            #print((y1_a_star, y2_a_star,x1_a_star, x2_a_star))

            self.a_star_safe_drive[y1_a_star:y2_a_star,x1_a_star:x2_a_star] = self.a_star_safe_drive[y1_a_star:y2_a_star,x1_a_star:x2_a_star] + self.a_star_pre_calculated_padding_matrix[0:y2_a_star-y1_a_star,0:x2_a_star-x1_a_star]
            self.regular_map[y,x] += 1
        return new_points

    def scan_match(self, source, pose):
        #t0 = time.time()
        if len(self.pcd.points) == 0 and not self.use_pre_scanned_map:
            self.pcd.points = o3d.utility.Vector3dVector(source)
            self.pcd.points.extend(self.update_gridmap(source))#self.pcd.voxel_down_sample(voxel_size=100)
            return pose, 0
        
        source_pcd = o3d.geometry.PointCloud()
        source_pcd.points = o3d.utility.Vector3dVector(source)

        #source_down = source_pcd
        # Downsample the point clouds
        source_pcd = source_pcd.voxel_down_sample(voxel_size=100)

        # Estimate normals for the downsampled point clouds
        source_pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.5, max_nn=30))
        self.pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.5, max_nn=30))

        trans_init = np.identity(4)
        reg_p2p = o3d.pipelines.registration.registration_icp(
            source_pcd, self.pcd,  max_correspondence_distance=1000, 
            init=trans_init, estimation_method=o3d.pipelines.registration.TransformationEstimationPointToPoint()
        )

        #print(reg_p2p)
        if reg_p2p.inlier_rmse > 200 or reg_p2p.fitness< 0.40:
            print(reg_p2p)
            print("SCRAPPING VALUES")
            return pose, 1

        source_pcd.transform(reg_p2p.transformation)

        if not self.use_pre_scanned_map:
            new_points = self.update_gridmap(source_pcd.points)
            self.pcd.points.extend(new_points)

        x, y =  self.rotate(pose[0] , pose[1], 0, 0, -reg_p2p.transformation[1][0])
        #print(time.time()-t0)
        return (x + reg_p2p.transformation[0][3] , y - reg_p2p.transformation[1][3], pose[2]-reg_p2p.transformation[1][0]), 0