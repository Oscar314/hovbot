from pyrplidar import PyRPlidar
import signal
import math
import time

class Lidar():
    def __init__(self):
        self.corners = []
        signal.signal(signal.SIGINT, self.signal_handler) 

    def signal_handler(self, sig, frame):
        lidar = PyRPlidar()
        lidar.connect(port="/dev/ttyUSB0", baudrate=256000, timeout=3)
        lidar.stop()
        lidar.set_motor_pwm(0)
        lidar.disconnect()
        exit(0)

    def scan(self, q, screen = None):
        lidar = PyRPlidar()
        lidar.connect(port="/dev/ttyUSB0", baudrate=256000, timeout=3)
        lidar.set_motor_pwm(500) 

        scan_generator =  lidar.start_scan_express(0)#lidar.force_scan(0) #lidar.start_scan_express(0) # lidar.force_scan() 
        current_scans = []
        for scan in scan_generator():
            try:
                current_scans.append(scan)
                if scan.start_flag:
                    angle_distance = [time.time()]
                    for s in current_scans:
                        if s.distance > 100:
                            angle_distance.append((s.distance, (s.angle-270)/180*math.pi))

                    current_scans = []
                    if q is not None:
                        q.put(angle_distance)

            except:
                print("send failed")
                current_scans = []
        print("Lidar exited!")
        return []
