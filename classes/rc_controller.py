import evdev
import time
from os import listdir
from threading import Lock

class Controller:
    def __init__(self, max_input, min_input):
        self.everything_ok = True
        self.max_input = max_input
        self.min_input = min_input
        self.speed_values = {"turn":0,"speed":0}
        self.save_map = False
        self.events = []
        self.lock = Lock()

    def get_status(self):
        self.lock.acquire()
        temp = self.everything_ok
        self.lock.release()
        return temp

    def get_speed_values(self):
        self.lock.acquire()
        temp = {"turn":self.speed_values["turn"], "speed":self.speed_values["speed"]}
        save = self.save_map
        self.save_map = False
        self.lock.release()
        return save, temp



class PS_controller(Controller):
    def __init__(self):
        super().__init__(255, 0)
        self.mapping = {4: "speed", 3: "turn"} #{5: "speed", 2: "turn", 305:"continue", 304:"cancel"}#
        self.inverse_mapping = {"speed":4, "turn":3}
        self.joystick = 3
        self.dir = "/dev/input/"
        self.names = ["Wireless Controller", "Sony Interactive Entertainment Wireless Controller"]
        self.gamepad = self.find_joystick()

    def update(self): 
        """Endless loop that runs in a separate thread to read input events.
            If connection to controller is lost this function has to set 
            self.everything_ok to False."""
        try:
            for event in self.gamepad.read_loop(): #Yields input event as they happen
                if event.type == 1 and event.code == 304:
                    self.save_map = True
                elif event.value != -1 and event.type == self.joystick:
                    if event.code == 4:
                        self.lock.acquire()
                        self.speed_values["speed"] = event.value
                        self.lock.release()
                    elif event.code == 3:
                        self.lock.acquire()
                        self.speed_values["turn"] = event.value
                        self.lock.release()                  
                    
        except:
            print("Connection to controller lost")
            self.lock.acquire()
            self.everything_ok = False
            self.lock.release()
        

    def center_joystick(self):
        """Make sure the controller is centered before starting."""
        print("Center joystick")
        while abs(self.speed_values["turn"] - self.min_input - (self.max_input-self.min_input)/2) > 10 or abs(self.speed_values["speed"] -self.min_input - (self.max_input-self.min_input)/2) > 10:
            time.sleep(0.1)
        
        print("joystick centered")

    def find_joystick(self):
        """Finds a joystick-file with the specific name."""
        gamepad = -1
        files = [f for f in listdir(self.dir)]
        print(files)
        if "js0" not in files:
            print("No joystick found, returning")
            exit()

        for f in [i for i in files if i[0:5] == "event"]:
            gamepad = evdev.InputDevice(self.dir+f)
            print(gamepad)
            
            if gamepad.name in self.names:
                print(f"Joystick found at file {f}")
                return gamepad
            gamepad = -1

        print("No eventfile for joystick found, stopping program")
        exit()