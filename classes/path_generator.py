import numpy as np

def square_path(x_start = 0, y_start = 0):
    way_points = []
    size = 100
    for x in range(x_start,x_start+size, 20):
        way_points.append([x, y_start])
    for x in range(y_start,y_start+size, 20):
        way_points.append([x_start+size, x]) 
    for x in range(x_start+size, x_start, -20):
        way_points.append([x, y_start+size]) 
    for x in range(y_start+size,y_start, -20):
        way_points.append([x_start, x])

    return False, way_points

def square_path1(x_start = 0, y_start = 0):
    way_points = []
    size = 200
    for x in range(x_start,x_start+size, 20):
        way_points.append([x, y_start])
    for x in range(y_start,y_start+size, 20):
        way_points.append([x_start+size, x]) 
    for x in range(x_start+size, x_start, -20):
        way_points.append([x, y_start+size]) 
    for x in range(y_start+size,y_start, -20):
        way_points.append([x_start, x])

    return False, way_points

def test_path():
    way_points = []
    for x in range(0, 100, 20):
        way_points.append([-x, x])

    for x in range(100, 200, 20):
        way_points.append([-100, x])

    for x in range(-100, 0, 20):
        way_points.append([x, 200])

    for x in range(0, 100, 20):
        way_points.append([x, 200+x])

    for x in range(0, 150, 20):
        way_points.append([100-x, 300])
    print(way_points)
    return False, way_points

def back_path():
    way_points = []
    for x in range(0, -100, -20):
        way_points.append([x, 0])
    for x in range(-100, 0, 20):
        way_points.append([x, 0])
    return True, way_points

def sine_path():
    way_points = []
    for x in range(0, 100, 20):
        way_points.append([x, 200])
    for x in range(100, 700, 20):
        y = 200 + 100*np.sin(2*np.pi*0.25*(x + 100)/100)
        way_points.append([x, int(y)])

def circle_path():
    way_points = []
    for x in range(0, 360, 10):
        way_points.append([50*np.sin(x*np.pi/180), 50*np.cos(x*np.pi/180)-50])

    return True, way_points

