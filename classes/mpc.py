import numpy as np
from scipy.optimize import minimize
import math

class MPC:
	def __init__(self, horizon, dt, matcher):
		self.horizon = horizon
		self.R = np.diag([0.01, 0.1])# input cost matrix
		self.dt = dt
		self.matcher = matcher


	def update(self, linear_velocity, angular_velocity, dt, x):
		return (linear_velocity*np.sin(x[2] + np.pi/2)*dt + x[0], 
				linear_velocity*np.cos(x[2] + np.pi/2)*dt + x[1], 
				-angular_velocity*dt + x[2])

	def cost(self, u_k, new_path, x_tup):
		u_k = u_k.reshape(self.horizon, 2).T
		cost = 0.0
		for i in range(self.horizon):
			x_tup = self.update(u_k[0,i], u_k[1,i], self.dt, x_tup)

				
			cost += np.sum(self.R@(u_k[:,i]**2))
			cost += (new_path[i][0]-x_tup[0])**2 + (new_path[i][1]-x_tup[1])**2 +((new_path[i][2]-x_tup[2])%(math.pi*2))**2
		return cost

	def optimize(self, points, x_tup):
		current = [x_tup[0], x_tup[1]]
		new_path = []
		for point in points:
			dx = point[0] - current[0]
			dy = point[1] - current[1]
			angle = math.atan2(dy, dx)%(math.pi*2)
			current = point
			new_path.append([point[0], point[1], angle])



		bnd = [(-20, 20),(np.deg2rad(-180), np.deg2rad(180))]*self.horizon
		result = minimize(self.cost, args=(new_path, x_tup), x0 = np.zeros((2*self.horizon)), method='SLSQP', bounds = bnd, options={'maxiter':10})
		return result.x[0],result.x[1], result.fun
