import pickle
import time

if __name__ == "__main__":
    import pygame
    pygame.init()

    with open("map2", 'rb') as f:
        a_star_safe_drive_map = pickle.load(f)

    with open("map1", 'rb') as f:
        regular_map = pickle.load(f)


    screen = pygame.display.set_mode([600,600])
    screen.fill((0, 0, 0))

    for i in range(600):
        for j in range(600):
            screen.set_at((i, j), (min(255, regular_map[i,j]*10), 0, 0)) #Change between regular_map -> a_star_safe_drive_map
    

    pixels = pygame.surfarray.pixels2d(screen)
    pixels[pixels == 0] = 20
    pixels[pixels == 85] = 200
    del pixels

    while True:
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
        time.sleep(0.1)

