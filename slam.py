from multiprocessing import Process, Queue
from classes.matcher import Matcher
from classes.a_star import A_star
from classes.hoverboard import Hoverboard
from classes.mpc import MPC
from classes.path_generator import circle_path
from classes.lidar import Lidar
import numpy as np
import time
import math
import pickle 


class Slam():
    def __init__(self):
        self.lidar = Lidar()
        self.lidar_q = Queue()
        self.lidar_p = Process(target=self.lidar.scan, args=(self.lidar_q,))
        self.lidar_p.start()

        self.hoverboard = Hoverboard()

        self.current_path = []

        self.pre_scanned_map = False
        if self.pre_scanned_map:
            self.current_path = []

            self.continuous = True
            with open("scans/map1", 'rb') as f:
                p = pickle.load(f)
            indices = zip(np.where(p > 0)[0], np.where(p > 0)[1])

            pts = []
            for x, y in indices:
                pts.append(np.array([(y-300)*50, (-x+300)*50, 0]))

            self.match = Matcher(use_pre_scanned_map=True, pre_scanned_map=np.array(pts))
        else:
            self.match = Matcher(use_pre_scanned_map=False, pre_scanned_map=[])
            self.continuous = False

        self.a_star = A_star()
        
        self.dt = 0.5
        self.horizon = 3
        self.current_idx_mpc = 0
        self.error_count = 0 
        self.controller_mpc = MPC(horizon = self.horizon, dt = self.dt, matcher = self.match)

            

    def observe_lidar(self):
        z = []
        while not self.lidar_q.empty():
            z = self.lidar_q.get()
        if len(z) > 0:
            return z[0], z[1:]
        else:
            return 0, []

    def save_lst(self, s, thing):
        with open(s, 'wb') as f:
            pickle.dump(thing, f)


    def mpc(self, path):
        x = (self.hoverboard.pose.x/10, self.hoverboard.pose.y/(-10), self.hoverboard.pose.theta)   #X, Y, Theta
        linear_v, angular_v, cost = self.controller_mpc.optimize(points = path[self.current_idx_mpc:self.current_idx_mpc+self.horizon], x_tup = x)

        if not self.pre_scanned_map:
            if not self.path_ok():
                self.create_path()

        goal_pt = path[self.current_idx_mpc]

        dist = math.hypot(x[0] - goal_pt[0], x[1] - goal_pt[1])
        #print((dist, abs(linear_v), abs(angular_v)))

        if dist<15 or (dist < 50 and abs(linear_v) < 5 and abs(angular_v) < 1):
            self.current_idx_mpc  += 1
            if self.current_idx_mpc + self.horizon > int(len(path)/2) and not self.continuous:
                print("At home")
                self.hoverboard.set_wheel_speeds(0,0)
                exit(0)
            else:
                self.current_idx_mpc %= int(len(path)/2)

        wheel_speed = (linear_v + np.pi*angular_v*2, linear_v - np.pi*angular_v*2)
        self.hoverboard.set_wheel_speeds(wheel_speed[0]/100, wheel_speed[1]/100)

    def create_path(self):
        self.hoverboard.set_wheel_speeds(0,0)
        a_star_path = self.a_star.astar(self.match.a_star_safe_drive, (300+int(self.hoverboard.pose.y/50),300+int(self.hoverboard.pose.x/50)), (15,15))

        self.current_path = []
        self.current_idx_mpc = 0
        if a_star_path != -1:
            for y, x in a_star_path:
                self.current_path.append(((x-300)*5, (-y+300)*5))
            self.save_lst("scans/path", a_star_path)
            print("New path")
        
        else:
            print("no path exists, going home!")
            a_star_path = self.a_star.astar(self.match.a_star_safe_drive, (300+int(self.hoverboard.pose.y/50),300+int(self.hoverboard.pose.x/50)), (300,300))
            if a_star_path != -1:
                for y, x in a_star_path:
                    self.current_path.append(((x-300)*5, (-y+300)*5))
                self.save_lst("scans/path", a_star_path)
                print("Going home")

            else:
                print("Cant go home")
                self.hoverboard.set_wheel_speeds(0,0)
                exit(0)

    def path_ok(self):
        for current_p in self.current_path[self.current_idx_mpc*2:min(len(self.current_path), 2*self.current_idx_mpc+25)]:
            if self.match.a_star_safe_drive[300 - int(current_p[1]/5), 300 + int(current_p[0]/5)] != 0:
                return False
        return True

    def real_loop(self):
        z_time, z = self.observe_lidar()
        if len(z) < 100:
            print("To few lidar measurements")
            return
        
        self.hoverboard.update_pose()
        actual_pose, map_error = self.match.add_scan((self.hoverboard.pose.x, self.hoverboard.pose.y, self.hoverboard.pose.theta),  z)

        if map_error:
            self.error_count += 1
            if self.error_count > 5:
                self.hoverboard.set_wheel_speeds(0,0)
                print("Too many map-errors :(")
                exit(0)
        else:
            self.error_count = 0
        
            self.hoverboard.pose.x = actual_pose[0]
            self.hoverboard.pose.y = actual_pose[1]
            self.hoverboard.pose.theta = actual_pose[2]

        if not self.pre_scanned_map:
            self.save_lst("scans/map2", self.match.a_star_safe_drive)
            self.save_lst("scans/map1", self.match.regular_map)

        
        #if self.v2x is not None:
        #    self.current_path = self.v2x.get_path()
        if len(self.current_path) == 0:
            if self.pre_scanned_map:
                self.hoverboard.set_wheel_speeds(0,0)
                print("Yuu have to provide a path if using a prescanned map")
                exit(0)
            else:
                self.create_path()

        self.mpc((self.current_path + self.current_path)[::2]) #Ignore every other point



if __name__ == "__main__":
    running = True
    slam = Slam()
    slam.hoverboard.set_odrive()

    goal_time = time.time()
    while running:
        goal_time += slam.dt
        sleep_time = max(0, goal_time-time.time())
        if sleep_time == 0:
            print(f"Slam running slow! {goal_time-time.time()}")
            goal_time = time.time()
        else:
            time.sleep(sleep_time)

        x = time.time()
        slam.real_loop()
        #print(f"slam time: {time.time()-x}")
       

   