from odrive.enums import AXIS_STATE_IDLE, AXIS_STATE_CLOSED_LOOP_CONTROL, CONTROL_MODE_VELOCITY_CONTROL, INPUT_MODE_VEL_RAMP, AXIS_STATE_FULL_CALIBRATION_SEQUENCE
import classes.constants as constants
import classes.rc_controller as co
from threading import Thread
import math
import time
import sys
import signal
import odrive

class State:
    def __init__(self, x=0.0, y=0.0, theta=0.0):
        self.x = x
        self.y = y
        self.theta = theta
        self.linear_v = 0
        self.angular_v = 0

class Hoverboard():
    def __init__(self):
        signal.signal(signal.SIGINT, self.signal_handler)
        self.controller = None
        self.wheelbase = constants.track_width
        self.pose = State()
        self.my_drive = None
        self.last_axis0 = 0
        self.last_axis1 = 0

    def find_and_cal_odrive(self, calibrate = False):
        """Connects to the odrive and calibrates if calibrate == True."""
        print("finding an odrive...") 
    
        my_drive = odrive.find_any()
        print("Odrive connected, bus voltage is " + str(my_drive.vbus_voltage) + "V")
    
        if calibrate:
            print("starting calibration...")
            my_drive.axis0.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE
            my_drive.axis1.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE
  
            while my_drive.axis0.current_state != AXIS_STATE_IDLE or my_drive.axis1.current_state != AXIS_STATE_IDLE:
                time.sleep(0.1)
  
            print("calibration finished")
        self.setup_vel_control(my_drive.axis0, my_drive.axis1)
        print("motors enabled")
        return my_drive
    
    def setup_vel_control(self, axisR, axisL):
        print("Activating velocity control")
        axisR.controller.input_vel = 0
        axisL.controller.input_vel = 0
        axisR.encoder.set_linear_count(0)
        axisL.encoder.set_linear_count(0)
        axisR.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
        axisL.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL

        axisR.controller.config.control_mode = CONTROL_MODE_VELOCITY_CONTROL # Set to position control
        axisL.controller.config.control_mode = CONTROL_MODE_VELOCITY_CONTROL # Set to position control

        axisR.controller.config.vel_ramp_rate = constants.vel_ramp_rate
        axisL.controller.config.vel_ramp_rate = constants.vel_ramp_rate

        axisR.controller.config.vel_integrator_gain = constants.vel_gain_I
        axisL.controller.config.vel_integrator_gain = constants.vel_gain_I

        axisR.controller.config.vel_gain = constants.vel_gain_P
        axisL.controller.config.vel_gain = constants.vel_gain_P
        axisR.controller.config.input_mode = INPUT_MODE_VEL_RAMP
        axisL.controller.config.input_mode = INPUT_MODE_VEL_RAMP


    def set_odrive(self):
        self.my_drive = self.find_and_cal_odrive(calibrate = False)
        self.last_axis0 =  self.my_drive.axis0.encoder.pos_estimate*constants.right_wheel_circ
        self.last_axis1 = -self.my_drive.axis1.encoder.pos_estimate*constants.left_wheel_circ


    def stop(self):
        try:
            self.my_drive.axis0.requested_state = AXIS_STATE_IDLE
            self.my_drive.axis1.requested_state = AXIS_STATE_IDLE
            print("Motors disabled")
        except:
            print("No connection to odrive")
            print("Shutting down")
            exit()

    def setSpeed(self):#Mixes turn and speed values from controller
        save, speed_dict = self.controller.get_speed_values()
        turn = speed_dict["turn"]
        speed = speed_dict["speed"]

        speed = max(min(speed, self.controller.max_input), self.controller.min_input)
        turn = max(min(turn, self.controller.max_input), self.controller.min_input)
        speed -= self.controller.max_input/2
        turn -= self.controller.max_input/2
        
        if abs(speed) < self.controller.max_input*0.05:
            speed = 0
        if abs(turn) < self.controller.max_input*0.05:
            turn = 0
        
        speed /= self.controller.max_input*constants.right_wheel_circ/(2*constants.max_vel)
        turn /= self.controller.max_input*constants.right_wheel_circ/(2*constants.max_vel)
        self.my_drive.axis0.controller.input_vel = -speed - constants.turn_bias*turn  # Sätter hastighet i rotationer/s
        self.my_drive.axis1.controller.input_vel = speed - constants.turn_bias*turn  #left

    def set_wheel_speeds(self, left, right): #Sets the wheel speeds in m/s
        self.my_drive.axis0.controller.input_vel = right*1000/(constants.right_wheel_circ)
        self.my_drive.axis1.controller.input_vel = -left*1000/(constants.left_wheel_circ)


    def get_distances(self):
        a0 = self.my_drive.axis0.encoder.pos_estimate*constants.right_wheel_circ
        a1 = self.my_drive.axis1.encoder.pos_estimate*constants.left_wheel_circ
        dr = a0- self.last_axis0
        dl = -a1 - self.last_axis1
        self.last_axis0 = a0
        self.last_axis1 = -a1
        return dr, dl

    def update_pose(self):
        dr, dl = self.get_distances()
        self.pose.theta = (self.pose.theta + (dr - dl)/ self.wheelbase)%(math.pi*2)
        d = (dl+dr)/2
        self.pose.x += d*math.cos(self.pose.theta)
        self.pose.y += d*math.sin(self.pose.theta)

    def slam_loop(self):
        try:
            self.setSpeed()
        except:
            self.stop()


    def signal_handler(self, sig, frame):
        self.stop()
        sys.exit(0)


    def set_controller(self):
        self.controller = co.PS_controller()
        self.control_thread = Thread(target=self.controller.update)
        self.control_thread.daemon = True
        self.control_thread.start()
        self.controller.center_joystick()



    def hoverboard_process_rc(self):
        try:
            self.set_odrive()
            self.set_controller()

            goal_time = time.time()
            f = 5 #Hz
            while True:
                if not self.controller.everything_ok:
                    print("Controller thread reported error, exiting")
                    raise Exception("Controller thread reported error, exiting")
                goal_time += 1/f
                sleep_time = max(0, goal_time-time.time())
                time.sleep(sleep_time)
                if sleep_time == 0:
                    print("Hoverboard running slow")
                
                self.setSpeed()

        except:
            self.stop()

if __name__ == "__main__": 
    hoverboard = Hoverboard()
    hoverboard.hoverboard_process_rc()